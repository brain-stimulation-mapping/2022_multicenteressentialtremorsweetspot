% Probabilistic Stimulation Map Multicentric Tremor
% Rev 01 thuyanhkhoa.nguyen@insel.ch, February 2021
% Rev 01B Feb 23, 2021; using efficiency 
% Rev 02 March 18, 2021, after meeting, higher cut-off, lower alpha for
% one-tailed tests, permutation
% Rev 03 March 25, 2021; optimizations; changing from two one-sided t-tests to a one
% two-sided t-test; preflipped all left VTAs to right
% Rev 04, April 2021, checking the significant clusters for isolated voxels
% and remove them (each voxel needs at least one neighbor)
% Rev 04 Part A, April 2021; adding ten Amsterdam patients with part A only
% Rev 05, October 2021; after Annals review
% Rev 06, December 2021; after second Annals review, slightly different
% correction factor for preop tremor baseline

clearvars
close all
tic

%% 
markerSize = 6;
printImages = false;
plotImages = true;
fileAtlas = ...
    'anatomyDISTALminimal.ply';
colorIdx = 4; % color code for STN in DISTAL atlas
ptCloud = pcread( fileAtlas );
pcColors = unique( ptCloud.Color,'rows' );
tmpIdx = ptCloud.Color == pcColors( colorIdx,: );
Idx = sum( tmpIdx,2 ) == 3;
IdxLeft = ptCloud.Location( :,1 ) < 0;
IdxRight = ptCloud.Location( :,1 ) >= 0;
[ boundarySTNLeft, ~ ] = boundary( ...
    double( ptCloud.Location( (Idx & IdxLeft ), 1 )), ...
    double( ptCloud.Location( (Idx & IdxLeft ), 2 )), ...
    double( ptCloud.Location( (Idx & IdxLeft ), 3 )), 0.25);
[ boundarySTNRight, ~ ] = boundary( ...
    double( ptCloud.Location( (Idx & IdxRight ), 1 )), ...
    double( ptCloud.Location( (Idx & IdxRight ), 2 )), ...
    double( ptCloud.Location( (Idx & IdxRight ), 3 )), 0.25);

fhMeanEffect = figure('color','w');
fhSignificantBetter = figure('color','w');
fhSignificantWorse = figure('color','w');

% refresh figures
figure( fhMeanEffect )
hold off
trisurf( boundarySTNRight, ptCloud.Location( ( Idx & IdxRight ), 1 ), ...
    ptCloud.Location( ( Idx & IdxRight ) , 2 ), ...
    ptCloud.Location( ( Idx & IdxRight ) , 3 ), 'Facecolor', [ .4 .4 .4 ],...
    'FaceAlpha', 0.2, 'LineStyle' , 'none' , 'EdgeColor' , pcColors( colorIdx, : ) );
xlabel('Medial-lateral (mm)')
ylabel('Posterior-anterior (mm)')
zlabel('Inferior-superior (mm)')
grid on
xlim([0 20]); ylim([-20 -5]); zlim([-15 0]);
hold on

figure( fhSignificantBetter )
hold off
trisurf( boundarySTNRight, ptCloud.Location( ( Idx & IdxRight ), 1 ), ...
    ptCloud.Location( ( Idx & IdxRight ) , 2 ), ...
    ptCloud.Location( ( Idx & IdxRight ) , 3 ), 'Facecolor', [ .4 .4 .4 ],...
    'FaceAlpha', 0.2, 'LineStyle' , 'none' , 'EdgeColor' , pcColors( colorIdx, : ) );
xlabel('Medial-lateral (mm)')
ylabel('Posterior-anterior (mm)')
zlabel('Inferior-superior (mm)')
grid on
xlim([0 20]); ylim([-20 -5]); zlim([-15 0]);
hold on

figure( fhSignificantWorse )
hold off
trisurf( boundarySTNRight, ptCloud.Location( ( Idx & IdxRight ), 1 ), ...
    ptCloud.Location( ( Idx & IdxRight ) , 2 ), ...
    ptCloud.Location( ( Idx & IdxRight ) , 3 ), 'Facecolor', [ .4 .4 .4 ],...
    'FaceAlpha', 0.2, 'LineStyle' , 'none' , 'EdgeColor' , pcColors( colorIdx, : ) );
xlabel('Medial-lateral (mm)')
ylabel('Posterior-anterior (mm)')
zlabel('Inferior-superior (mm)')
grid on
xlim([0 20]); ylim([-20 -5]); zlim([-15 0]);
hold on

%%
createTremorReductionTablePartARev05; % table and numberVTAs
tableTremorReductionOriginal = tableTremorReduction;
tableTremorReduction = readtable( '../../03_Data/ForAnnalsReview01/Multivariate_Analysis.xlsx', 'Sheet', 'TableRearrangedForMatlab' );
tableTremorReduction = [ tableTremorReduction tableTremorReductionOriginal.fileNames ];
tableTremorReduction.Properties.VariableNames( end ) = { 'fileNames' };
tableTremorReduction.centerID = categorical( tableTremorReduction.centerID );
tableTremorReduction.leadID = categorical( tableTremorReduction.leadID );
tableTremorReduction.tremorReductionPartA = tableTremorReduction.tremorReductionPartA/100;

VOXELSIZE = [.25 .25 .25];
probabilisticMapAllCenters = ea_load_nii( 'anatT2Resliced.nii' );
mapSizeX = 65; % 5.5 .. 21.25 ; 5 .. 25
mapSizeY = 80; % -24.75 .. -6; -25 .. -5
mapSizeZ = 130; % -16.25 .. 15; -16.5 .. 16
probabilisticMapAllCenters.img = zeros( mapSizeX, mapSizeY, mapSizeZ );
probabilisticMapAllCenters.dim = [ mapSizeX, mapSizeY, mapSizeZ ];
probabilisticMapAllCenters.mat( :,4 ) = [ 5.25; -25; -16.5; 1 ]; % set origin
nImage = probabilisticMapAllCenters;
singleClinicalScores = single( NaN( mapSizeX * mapSizeY * mapSizeZ, numberVTAs ) );
centerBalancingFactor = ones( 5, 1 );
% centerBalancingFactor = height( tableTremorReduction )/5 ...
%     *[ 1/72 1/34 1/34 1/37 1/37 ]; % Charite, Insel, Cologne, Oxford, Amsterdam

for indexVTA = 1:height( tableTremorReduction )
    fileNameVTA = tableTremorReduction.fileNames{ indexVTA };
    VTANifti = ea_load_nii( fileNameVTA );
%     VTANifti.img( isnan( VTANifti.img ) ) = 0;
%     VTANifti.img = round( VTANifti.img ); % clean up
    indexLinearVTA = find( VTANifti.img >= 0.33 );
    [ xx, yy, zz ] = ind2sub( size( VTANifti.img ), indexLinearVTA );
    if ~isempty( xx )
        VTAVoxelCoordinates = [ xx, yy, zz ];
        VTAWorldCoordinates = mapVoxelToWorld( VTAVoxelCoordinates, VTANifti );
        % map from millimeter world coordinates to voxel coordinates of container
        aggregatedVTAsVoxelCoordinates = mapWorldToVoxel( VTAWorldCoordinates, probabilisticMapAllCenters );
        indexLinear = sub2ind( size( probabilisticMapAllCenters.img ),... % get linear index
            aggregatedVTAsVoxelCoordinates( :, 1 ),...
            aggregatedVTAsVoxelCoordinates( :, 2 ),...
            aggregatedVTAsVoxelCoordinates( :, 3 ));
        
        probabilisticMapAllCenters.img( unique( indexLinear ) ) ...
            = probabilisticMapAllCenters.img( unique( indexLinear ) ) ...
            + centerBalancingFactor( tableTremorReduction.centerID( indexVTA ) ) * ( ...
            + VTANifti.img( indexLinearVTA ) ...
            * tableTremorReduction.tremorReductionPartA( indexVTA ) ...
            - 2.2475e-2 * tableTremorReduction.preopTremorPartA( indexVTA )); % correction for preop baseline, rev05 1.5568
        % / tableTremorReduction.TEEDuW( indexVTA );
        singleClinicalScores( unique( indexLinear ) , indexVTA ) ...
            = ...
            + centerBalancingFactor( tableTremorReduction.centerID ( indexVTA ) ) * ( ...
            VTANifti.img( indexLinearVTA ) ...
            * tableTremorReduction.tremorReductionPartA( indexVTA ) ...
            - 2.2475e-2 * tableTremorReduction.preopTremorPartA( indexVTA )); 
        % / tableTremorReduction.TEEDuW( indexVTA );
        nImage.img( unique( indexLinear ) ) = ...
            nImage.img( unique( indexLinear ) ) + 1;
    end
end

% discard voxels
activationCutOff = 4;
voxelsToDiscard = find( nImage.img < activationCutOff );
nImage.img( voxelsToDiscard ) = 0;
probabilisticMapAllCenters.img( voxelsToDiscard ) = 0;
singleClinicalScores( voxelsToDiscard, : ) = NaN( numel( voxelsToDiscard ), numberVTAs );

% averaging
voxelsToAverage = find( nImage.img >= activationCutOff );
probabilisticMapAllCenters.img( voxelsToAverage ) = ...
    probabilisticMapAllCenters.img( voxelsToAverage )./ ...
    nImage.img( voxelsToAverage ); % calculating mean

if plotImages
    voxelsToPlot = find( nImage.img >= activationCutOff );%voxelsToAverage;
    [ xx, yy, zz ] = ind2sub( size( probabilisticMapAllCenters.img ), voxelsToPlot );
    if ~isempty( xx )
        voxelCoordinates = [ xx, yy, zz ]; % concatenate points to one matrix.
        worldCoordinates = mapVoxelToWorld( voxelCoordinates, probabilisticMapAllCenters ); % map to mm-space
    end
    figure( fhMeanEffect )
    h = scatter3( ...
        worldCoordinates( :,1 ), ...
        worldCoordinates( :,2 ), ...
        worldCoordinates( :,3 ), ...
        markerSize,...
        probabilisticMapAllCenters.img( voxelsToPlot ),'filled' );
    h.MarkerFaceAlpha = 0.4;
    
    view( [ -30,30 ] )
    cb = colorbar;
    caxis( [ 0 max( probabilisticMapAllCenters.img( voxelsToPlot ) ) ] )
    cb.Label.String = 'Mean clinical improvement';
    title( 'Mean clinical improvement image' )
end

tmpNifti = make_nii( nImage.img, nImage.voxsize, mapWorldToVoxel( [0 0 0], nImage ) );
save_nii( tmpNifti, 'PSMRev06/nImage.nii' );
tmpNifti = make_nii( probabilisticMapAllCenters.img, probabilisticMapAllCenters.voxsize, mapWorldToVoxel( [0 0 0], probabilisticMapAllCenters ) );
save_nii( tmpNifti, 'PSMRev06/meanImprovement.nii' );

%% Statistical testing

voxelsToTest = find( nImage.img >= activationCutOff );
voxelsToTest = sort( voxelsToTest );
voxelsToTestTrimmed = ( 1:numel( voxelsToTest ) )';
singleClinicalScoresTrimmed = singleClinicalScores( voxelsToTest, : );
hContainerTrimmed = logical( false( numel( voxelsToTestTrimmed ), 1 ));
hContainerBetterTrimmed = logical( false( numel( voxelsToTestTrimmed ), 1 ));
hContainerWorseTrimmed = logical( false( numel( voxelsToTestTrimmed ), 1 ));
hContainerBetter = uint8( zeros( size( nImage.img )));
hContainerWorse = uint8( zeros( size( nImage.img )));
pValueContainerTrimmed = zeros( numel( voxelsToTestTrimmed ), 1 );
pValueContainerBetterTrimmed = zeros( numel( voxelsToTestTrimmed ), 1 );
pValueContainerWorseTrimmed = zeros( numel( voxelsToTestTrimmed ), 1 );
tStatisticContainerBetterTrimmed = single( zeros( numel( voxelsToTestTrimmed ), 1 ));
tStatisticContainerWorseTrimmed = single( zeros( numel( voxelsToTestTrimmed ), 1 ));
alpha = 0.05; % two-sided p value is twice of one-sided p-value
threshold = prctile( singleClinicalScoresTrimmed( ~isnan( singleClinicalScoresTrimmed(:) )), 33 ); % 33 percentile

for indexVoxel = 1:numel( voxelsToTestTrimmed )
    voxelsPopulation = setdiff( voxelsToTestTrimmed, indexVoxel );
    populationScores = singleClinicalScoresTrimmed( voxelsPopulation, : );
%     [ hContainerTrimmed( indexVoxel ), pValueContainerTrimmed( indexVoxel ), ~ , stats ] = ...
%         ttest2( singleClinicalScoresTrimmed( indexVoxel, : ), ...
%         populationScores(:), ...
%         'alpha', alpha, 'tail', 'both');
%     [ hContainerTrimmed( indexVoxel ), pValueContainerTrimmed( indexVoxel ), ~ , stats ] = ...
%         signrank( singleClinicalScoresTrimmed( indexVoxel, : ), threshold, ...
%         'alpha', alpha, 'tail', 'both');
%         prctile( populationScores( ~isnan( populationScores(:) )), 33 ), ...
%     if hContainerTrimmed( indexVoxel )
%         if stats.tstat > 0
%             tStatisticContainerBetterTrimmed( indexVoxel ) = stats.tstat;
%             hContainerBetterTrimmed( indexVoxel ) = true;
%         else
%             tStatisticContainerWorseTrimmed( indexVoxel ) = stats.tstat;
%             hContainerWorseTrimmed( indexVoxel ) = true;
%         end
%     end
    [ pValueContainerBetterTrimmed( indexVoxel ), hContainerBetterTrimmed( indexVoxel ), ~ ] = ...
        signrank( singleClinicalScoresTrimmed( indexVoxel, : ), threshold, ...
        'alpha', alpha, 'tail', 'right');
    [ pValueContainerWorseTrimmed( indexVoxel ), hContainerWorseTrimmed( indexVoxel ), ~ ] = ...
        signrank( singleClinicalScoresTrimmed( indexVoxel, : ), threshold, ...
        'alpha', alpha, 'tail', 'left');
%     pValueContainerTrimmed( indexVoxel ) = min( ...
%         pValueContainerBetterTrimmed( indexVoxel ), ...
%         pValueContainerWorseTrimmed( indexVoxel ));
%     pValueContainerTrimmed( indexVoxel ) = ...
%         pValueContainerBetterTrimmed( indexVoxel );
end

% false discovery rate, see Genovese et al. (2002)
qFDR = 0.05;
numberOfVoxels = numel( voxelsToTest );
constantV = log( numberOfVoxels ) + 0.5772;
pValueBound = ( ( 1:numberOfVoxels )/numberOfVoxels * qFDR/constantV )';

pValuesSorted = sort( pValueContainerBetterTrimmed );
rFDR = find( pValuesSorted > pValueBound, 1, 'first' );
pValueBound = pValuesSorted( rFDR-1 );
voxelsInsignificant =  pValueContainerBetterTrimmed >= pValueBound ;
hContainerBetterTrimmed( voxelsInsignificant ) = false;

pValuesSorted = sort( pValueContainerWorseTrimmed );
rFDR = find( pValuesSorted > pValueBound, 1, 'first' );
if rFDR > 1
    pValueBound = pValuesSorted( rFDR-1 );
else
    pValueBound = pValuesSorted( 1 );
end
voxelsInsignificant = ( pValueContainerWorseTrimmed >= pValueBound );
hContainerWorseTrimmed( voxelsInsignificant ) = false;

for indexVoxel = 1:numel( voxelsToTestTrimmed )
    hContainerBetter( voxelsToTest( indexVoxel )) = ...
        hContainerBetterTrimmed( indexVoxel );
    hContainerWorse( voxelsToTest( indexVoxel )) = ...
        hContainerWorseTrimmed( indexVoxel );
end

mapSignificantBetter = probabilisticMapAllCenters;
mapSignificantBetter.img = zeros( mapSizeX, mapSizeY, mapSizeZ );
voxelsSignificantBetter = find( hContainerBetter == 1 );
mapSignificantBetter.img( voxelsSignificantBetter ) = probabilisticMapAllCenters.img( voxelsSignificantBetter );
mapSignificantBetterBinary = mapSignificantBetter;
mapSignificantBetterBinary.img( voxelsSignificantBetter ) = 1;

if ( plotImages ) && ( ~isempty( voxelsSignificantBetter ) )
    [ xx, yy, zz] = ind2sub( size( probabilisticMapAllCenters.img ), voxelsSignificantBetter );
    if ~isempty( xx )
        voxelCoordinates = [ xx, yy, zz ]; % concatenate points to one matrix.
        worldCoordinates = mapVoxelToWorld( voxelCoordinates, probabilisticMapAllCenters ); % map to mm-space
    end
    figure( fhSignificantBetter )
    h = scatter3( ...
        worldCoordinates( :,1 ), ...
        worldCoordinates( :,2 ), ...
        worldCoordinates( :,3 ), ...
        markerSize,...
        probabilisticMapAllCenters.img( voxelsSignificantBetter ),'filled' );
    h.MarkerFaceAlpha = 0.4;
    
    view( [ -30,30 ] )
    cb = colorbar;
    maxEffect = max( probabilisticMapAllCenters.img( voxelsSignificantBetter ) );
    caxis( [ 0 maxEffect ] )
    cb.Label.String = 'Mean clinical improvement';
    title( 'Significant better mean clinical improvement image' )
end

mapSignificantWorse = probabilisticMapAllCenters;
mapSignificantWorse.img = zeros( mapSizeX, mapSizeY, mapSizeZ );
voxelsSignificantWorse = find( hContainerWorse == 1 );
mapSignificantWorse.img( voxelsSignificantWorse ) = probabilisticMapAllCenters.img( voxelsSignificantWorse );

if ( plotImages ) && ( ~isempty( voxelsSignificantWorse ) )
    [ xx, yy, zz] = ind2sub( size( probabilisticMapAllCenters.img ), voxelsSignificantWorse );
    if ~isempty( xx )
        voxelCoordinates = [ xx, yy, zz ]; % concatenate points to one matrix.
        worldCoordinates = mapVoxelToWorld( voxelCoordinates, probabilisticMapAllCenters ); % map to mm-space
    end
    figure( fhSignificantWorse )
    h = scatter3( ...
        worldCoordinates( :,1 ), ...
        worldCoordinates( :,2 ), ...
        worldCoordinates( :,3 ), ...
        markerSize,...
        probabilisticMapAllCenters.img( voxelsSignificantWorse ),'filled');
    h.MarkerFaceAlpha = 0.4;
    
    view( [ -30,30 ] )
    cb = colorbar;
%     caxis( [ 0 max( probabilisticMapAllCenters.img( voxelsToPlot ) ) ] )
    caxis( [ 0 maxEffect ] );
    cb.Label.String = 'Mean clinical improvement';
    title( 'Significant worse mean clinical improvement image' )
end

tmpNifti = make_nii( mapSignificantBetter.img, mapSignificantBetter.voxsize, mapWorldToVoxel( [0 0 0], mapSignificantBetter ) );
% save_nii( tmpNifti, 'PSMRev05/significantBetterEfficiencyMulticenterTremor.nii' );
save_nii( tmpNifti, 'PSMRev06/significantBetterImprovement.nii' );
tmpNifti = make_nii( mapSignificantBetterBinary.img, mapSignificantBetterBinary.voxsize, mapWorldToVoxel( [0 0 0], mapSignificantBetterBinary ) );
save_nii( tmpNifti, 'PSMRev06/significantBetterImprovementBinary.nii' );
% tmpNifti = make_nii( mapSignificantWorse.img, mapSignificantWorse.voxsize, mapWorldToVoxel( [0 0 0], mapSignificantWorse ) );
% save_nii( tmpNifti, 'PSMRev05/mapSignificantWorseImprovement.nii' );
% save_nii( tmpNifti, 'PSMRev05/mapSignificantWorseEfficiencyMulticenterTremor.nii' );

toc