% 2006 Multicentric tremor map
% Create tremor reduction vector for PSM
% 2021-02-23, fixed bug in Oxford sorting 
% Rev05, October 2021, after Annals review

A = repmat( 'AMS', 17, 1 );
B = repmat( 'AMZ', 20, 1 );
C = repmat( 'CHA', 72, 1 );
D = repmat( 'COL', 34, 1 );
E = repmat( 'INS', 34, 1 );
F = repmat( 'OXF', 37, 1 );
Centers = [ A; B; C; D; E; F ];
numberVTAs = size( Centers, 1 );

%% Get patientIDs

PatientIDs = cell( numberVTAs, 1 );
fileNames = cell( numberVTAs, 1 );
baseFolder = '../../03_Data/TREMOR_Recons_VTA/VTAsReslicedForPSMRev05/';
centerFolders = dir( baseFolder );
centerFolders( 1:2 ) = []; % delete . and ..
counterVTA = 0;
for indexCenter = 1:numel( centerFolders )
    VTAfiles = dir([ baseFolder filesep centerFolders( indexCenter ).name ]);
    VTAfiles(1:2) = [];
    
    for indexVTA = 1:length( VTAfiles )
        counterVTA = counterVTA + 1;
        tmpString = strsplit( VTAfiles( indexVTA ).name, '_' );
        PatientIDs{ counterVTA } = tmpString{ 1 }; 
        fileNames{ counterVTA } = [ VTAfiles( indexVTA ).folder filesep VTAfiles( indexVTA ).name ];
    end
end

%% PART A ONLY
tremorReduction = [...
    0.833 0.429 0.444 0.000 0.714 1.000 -0.250 0.909 0.900 0.500 0.500 0.833 0.286 0.571 0.000 1.000 0.500 ... % AMS, see filenames as listed by Matlab not Nautilus or other
    0.333 0.800 0.600 0.750 1.000 1.000 0.625 0.625 0.667 0.800 0.857 1.000 1.000 1.000 1.000 1.000 0.750 0.667 0.833 0.778 ... % AMS 2 , part A only
    1.000 0.933 1.000 1.000 0.400 0.333 0.600 1.000 1.000 0.875 0.400 0.000 0.800 0.750 0.000 0.000 0.800 1.000 0.500 0.778 ...
    1.000 1.000 0.778 -1.000 0.500 0.500 1.000 0.833 0.800 0.833 0.750 0.500 0.800 0.833 0.400 0.286 1.000 0.625 0.625 0.875 ...
    1.000 1.000 0.250 0.250 0.750 0.667 0.000 0.800 0.600 0.500 0.500 0.667 0.500 0.500 0.857 0.875 0.923 0.933 0.714 0.143 ...
    0.500 0.500 0.800 0.667 0.750 0.750 1.000 1.000 0.750 0.833 0.750 0.875 ... % CHARITE
    0.600 0.667 1.000 0.667 0.000 0.400 0.857 0.800 1.000 1.000 ... % Cologne ET 15 17 18 23 3
    1.000 0.909 0.833 0.833 0.667 0.500 0.800 0.500 0.857 1.000 1.000 0.750 0.667 0.800 0.300 0.000 0.750 1.000 0.250 0.500 ...
    0.500 0.750 0.667 0.667 ... % Cologne P01-12
    0.167 0.167 0.333 0.308 0.333 0.444 0.500 0.571 0.667 0.833 1.000 1.000 0.500 0.429 0.500 0.333 0.429 0.571 0.375 0.286 ...
    1.000 1.000 0.286 0.167 0.778 0.714 0.500 0.000 0.250 0.571 0.429 0.875 0.000 0.250 ... % Insel
    0.278 0.765  .857 .833 .625 .667 .6 -.182 .417 .462 .667 .692 .733 ...
    .688 .8 .833 .8 1 1 .455 1 .556 .818 .333 .789 .765 .875 .786 .375 ...
    .733 .75 .211 .529 .167 .643 .667 .8 ]'; % OXF

stimulationStrength = [...
    2.6 2.9 2.1 3.3 2.2 2.1 2.3 2.4 2.6 3 1.2 1.9 2.6 2.1 2.3 2.4 2.1 ... % AMS
    3.7 2.8 2 3.5 2 2 2.7 2.4 1.1 1.6 0.3 0.5 1.6 2.5 2.6 1.6 3.1 3.3 3.5 3.5 ... % AMS 2
    2.5 1.5 2.4 1.9 2 2.1 1.5 2 3.6 2.4 2.8 2.8 3.1 2 2.4 1.6 5.5 2.5 0.85 2 ...
    3 3 1.7 2.4 2.5 1.5 2 3 3 1.8 2.7 2.8 1.7 1.7 2.25 2.5 4.5 3 1.4 3.1 2.3 ...
    2 2 2 2.9 2.4 2 2.3 3.3 3.15 3 3 2 2.3 3 4.5 2.6 2.8 5.5 3 2.5 3 2.7 3.4 ...
    3 2 2 3.1 2.8 2.4 2.3 2 ... % Charite
    1.3 2 4 5.7 1.6 1.9 2.5 4.2 1.7 1.8 ... % COL ET-03/23
    3.9 3.9 2.2 2.4 2.6 1.4 6 6 1.2 1.5 2.6 1.3 3.3 2.4 2.3 2.3 2.5 2.4 ...
    2.5 1 2.2 2.1 3 2.4 ...% COL P01-12
    3.7 2.6 2 1.6 1.5 2.9 3.3 2.3 1.7 1.8 2 3.5 2.3 1.9 1.9 2 2.7 2 1.5 ...
    4 3 4.9 2.3 2.9 2.3 2.2 2.8 2.2 1.6 1.5 1.6 2.1 3 2.7 ... % Insel
    2.5 1.5 2.6 2.7 3 2.9 1.75 2.8 1.4 3 2.8 2.4 2.4 2.8 2.1 3.2 3 3 2 ...
    4.4 4 3.4 1.7 3.9 4 3.5 1.4 1.8 1.8 3.2 2.5 1.7 2.4 4.2 1.8 3 2.6 ]'; % OXF

tableTremorReduction = table( Centers, PatientIDs, fileNames, tremorReduction, stimulationStrength );

