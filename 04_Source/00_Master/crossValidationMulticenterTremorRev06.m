% Multicenter tremor project
% leave-one-out cross-validation
% Rev01 May 2021, thuyanhkhoa.nguyen@insel.ch
% tried with sweet spot explorer, but buggy
% Rev02, using percentiles for map
% Rev03, p weighted mean efficiency map
% Rev03B, a variant after discussion with Andreas and Sabry on May 27, 2021
% trying classification, using set voxel values for worse, average and
% better
% Rev03D, same as Rev03B but with improvement
% Rev03E, same as Rev3D, adding overlap ratios to better, worse, average 
% also bugfix for better, worse voxels; did not check for significance
% Rev05, Nov 2021, after reviews from Annals of Neurology
% Rev06, Dec 2021, after second review from Annals, mainly slightly
% different correction factor for baseline tremor with nested LMM

clearvars
close all

markerSize = 6;
printImages = false;
plotImages = true;
fileAtlas = ...
    'anatomyDISTALminimal.ply';
colorIdx = 4; % color code for STN in DISTAL atlas
ptCloud = pcread( fileAtlas );
pcColors = unique( ptCloud.Color,'rows' );
tmpIdx = ptCloud.Color == pcColors( colorIdx,: );
Idx = sum( tmpIdx,2 ) == 3;
IdxLeft = ptCloud.Location( :,1 ) < 0;
IdxRight = ptCloud.Location( :,1 ) >= 0;
[ boundarySTNLeft, ~ ] = boundary( ...
    double( ptCloud.Location( (Idx & IdxLeft ), 1 )), ...
    double( ptCloud.Location( (Idx & IdxLeft ), 2 )), ...
    double( ptCloud.Location( (Idx & IdxLeft ), 3 )), 0.25);
[ boundarySTNRight, ~ ] = boundary( ...
    double( ptCloud.Location( (Idx & IdxRight ), 1 )), ...
    double( ptCloud.Location( (Idx & IdxRight ), 2 )), ...
    double( ptCloud.Location( (Idx & IdxRight ), 3 )), 0.25);

createTremorReductionTablePartARev05; % table and numberVTAs
tableTremorReductionOriginal = tableTremorReduction;
tableTremorReduction = readtable( '../../03_Data/Multivariate_Analysis.xlsx', 'Sheet', 'TableRearrangedForMatlab' );
tableTremorReduction = [ tableTremorReduction tableTremorReductionOriginal.fileNames ];
tableTremorReduction.Properties.VariableNames( end ) = { 'fileNames' };
tableTremorReduction.centerID = categorical( tableTremorReduction.centerID );
tableTremorReduction.leadID = categorical( tableTremorReduction.leadID );
tableTremorReduction.tremorReductionPartA = tableTremorReduction.tremorReductionPartA/100;
tableTremorReductionBase = tableTremorReduction;

VOXELSIZE = [.25 .25 .25];
mapProbabilistic = ea_load_nii( 'anatT2Resliced.nii' );
mapSizeX = 65; % 5.5 .. 21.25 ; 5 .. 25
mapSizeY = 80; % -24.75 .. -6; -25 .. -5
mapSizeZ = 130; % -16.25 .. 15; -16.5 .. 16
mapProbabilistic.img = zeros( mapSizeX, mapSizeY, mapSizeZ );
mapProbabilistic.dim = [ mapSizeX, mapSizeY, mapSizeZ ];
mapProbabilistic.mat( :,4 ) = [ 5.25; -25; -16.5; 1 ]; % set origin
nImage = mapProbabilistic;
singleClinicalScores = single( NaN( mapSizeX * mapSizeY * mapSizeZ, numberVTAs ) );
centerBalancingFactor = ones( 5, 1 );
% centerBalancingFactor = height( tableTremorReduction )/5 ...
%     *[ 1/72 1/34 1/34 1/37 1/37 ]; % Charite, Insel, Cologne, Oxford, Amsterdam

rng( 'default' )
numberFolds = 10;
crossValidationPartition = cvpartition( tableTremorReductionBase.centerID, 'KFold', numberFolds, 'Stratify', true );
crossValidationResults = zeros( height( tableTremorReductionBase ), 7 );

coordinatesClassicVIM = [ 13.1 -19.3 -2.1 ];
tractCologne = ea_load_nii( '../../03_Data/tracts/CologneDRTTResliced.nii' ); % thresholded DRTT 
tractCologne.img( tractCologne.img < 0.5 ) = 0;

for indexFold = 1:numberFolds
    tic
    tableTraining = tableTremorReductionBase( ...
        training( crossValidationPartition, indexFold ), : );
    tableTesting = tableTremorReductionBase( ...
        test( crossValidationPartition, indexFold ), : );
    numberVTAs = height( tableTraining );
   
    mapProbabilistic.img = zeros( mapSizeX, mapSizeY, mapSizeZ );
    nImage.img = zeros( mapSizeX, mapSizeY, mapSizeZ );
    singleClinicalScores = single( NaN( mapSizeX * mapSizeY * mapSizeZ, numberVTAs ) );
    counter = 0;
    
    fWaitMapping = waitbar( 0, sprintf( 'Map generation 0 percent' ) );

    for indexVTA = 1:height( tableTraining )
        waitbar( indexVTA/numberVTAs, fWaitMapping, sprintf( 'Map generation %d percent', round( indexVTA/numberVTAs*100 )) );
        fileNameVTA = tableTraining.fileNames{ indexVTA };
        VTANifti = ea_load_nii( fileNameVTA );
        indexLinearVTA = find( VTANifti.img >= 0.33 );
        [ xx, yy, zz ] = ind2sub( size( VTANifti.img ), indexLinearVTA );
        if ~isempty( xx )
            VTAVoxelCoordinates = [ xx, yy, zz ];
            VTAWorldCoordinates = mapVoxelToWorld( VTAVoxelCoordinates, VTANifti );
            % map from millimeter world coordinates to voxel coordinates of container
            aggregatedVTAsVoxelCoordinates = mapWorldToVoxel( VTAWorldCoordinates, mapProbabilistic );
            indexLinear = sub2ind( size( mapProbabilistic.img ),... % get linear index
                aggregatedVTAsVoxelCoordinates( :, 1 ),...
                aggregatedVTAsVoxelCoordinates( :, 2 ),...
                aggregatedVTAsVoxelCoordinates( :, 3 ));
            
            mapProbabilistic.img( unique( indexLinear ) ) ...
                = mapProbabilistic.img( unique( indexLinear ) ) ...
                + centerBalancingFactor( tableTraining.centerID( indexVTA ) ) * ( ...
                + VTANifti.img( indexLinearVTA ) ...
                * tableTraining.tremorReductionPartA( indexVTA ) ...
                - 2.2126e-2 * tableTraining.preopTremorPartA( indexVTA )); % correction for preop baseline, rev05 1.5568
            singleClinicalScores( unique( indexLinear ) , indexVTA ) ...
                = ...
                + centerBalancingFactor( tableTraining.centerID ( indexVTA ) ) * ( ...
                VTANifti.img( indexLinearVTA ) ...
                * tableTraining.tremorReductionPartA( indexVTA ) ...
                - 2.2126e-2 * tableTraining.preopTremorPartA( indexVTA ));
            nImage.img( unique( indexLinear ) ) = ...
                nImage.img( unique( indexLinear ) ) + 1;
        end
    end
    
    % discard voxels
    activationCutOff = 16;
    voxelsToDiscard = find( nImage.img < activationCutOff );
    nImage.img( voxelsToDiscard ) = 0;
    mapProbabilistic.img( voxelsToDiscard ) = 0;
    singleClinicalScores( voxelsToDiscard, : ) = NaN( numel( voxelsToDiscard ), numberVTAs );
    
    % averaging
    voxelsToAverage = find( nImage.img >= activationCutOff );
    mapProbabilistic.img( voxelsToAverage ) = ...
        mapProbabilistic.img( voxelsToAverage )./ ...
        nImage.img( voxelsToAverage ); % calculating mean
    close( fWaitMapping )
    
    %% Statistical testing
    
    voxelsToTest = find( nImage.img >= activationCutOff );
    voxelsToTest = sort( voxelsToTest );
    voxelsToTestTrimmed = ( 1:numel( voxelsToTest ) )';
    singleClinicalScoresTrimmed = singleClinicalScores( voxelsToTest, : );
    hContainerTrimmed = logical( false( numel( voxelsToTestTrimmed ), 1 ));
    hContainerBetterTrimmed = logical( false( numel( voxelsToTestTrimmed ), 1 ));
    hContainerWorseTrimmed = logical( false( numel( voxelsToTestTrimmed ), 1 ));
    hContainerBetter = uint8( zeros( size( nImage.img )));
    hContainerWorse = uint8( zeros( size( nImage.img )));
    pValueContainerTrimmed = zeros( numel( voxelsToTestTrimmed ), 1 );
    pValueContainerBetterTrimmed = zeros( numel( voxelsToTestTrimmed ), 1 );
    pValueContainerWorseTrimmed = zeros( numel( voxelsToTestTrimmed ), 1 );
    tStatisticContainerBetterTrimmed = single( zeros( numel( voxelsToTestTrimmed ), 1 ));
    tStatisticContainerWorseTrimmed = single( zeros( numel( voxelsToTestTrimmed ), 1 ));
    alpha = 0.05; % two-sided p value is twice of one-sided p-value
    threshold = prctile( singleClinicalScoresTrimmed( ~isnan( singleClinicalScoresTrimmed(:) )), 33 ); % 33 percentile

    fWaitStatistics = waitbar( 0, sprintf( 'Voxel-wise statistics 0 percent' ) );

    for indexVoxel = 1:numel( voxelsToTestTrimmed )
        waitbar( indexVoxel/numel( voxelsToTestTrimmed ), fWaitStatistics, ...
            sprintf( 'Voxel-wise statistics %d percent', round( indexVoxel/numel( voxelsToTestTrimmed )*100 )) );
%         voxelsPopulation = setdiff( voxelsToTestTrimmed, indexVoxel );
%         populationScores = singleClinicalScoresTrimmed( voxelsPopulation, : );
        [ pValueContainerBetterTrimmed( indexVoxel ), hContainerBetterTrimmed( indexVoxel ), ~ ] = ...
            signrank( singleClinicalScoresTrimmed( indexVoxel, : ), threshold, ...
            'alpha', alpha, 'tail', 'right');
%         [ pValueContainerBetterTrimmed( indexVoxel ), hContainerBetterTrimmed( indexVoxel ), ~ ] = ...
%             ranksum( singleClinicalScoresTrimmed( indexVoxel, : ), threshold, ...
%             'alpha', alpha, 'tail', 'right');
%         [ pValueContainerWorseTrimmed( indexVoxel ), hContainerWorseTrimmed( indexVoxel ), ~ ] = ...
%             signrank( singleClinicalScoresTrimmed( indexVoxel, : ), threshold, ...
%             'alpha', alpha, 'tail', 'left');
    end
    
    % false discovery rate, see Genovese et al. (2002)
    qFDR = 0.05;
    numberOfVoxels = numel( voxelsToTest );
    constantV = log( numberOfVoxels ) + 0.5772;
    pValueBound = ( ( 1:numberOfVoxels )/numberOfVoxels * qFDR/constantV )';
    
    pValuesSorted = sort( pValueContainerBetterTrimmed );
    rFDR = find( pValuesSorted > pValueBound, 1, 'first' );
    if rFDR > 1
        pValueBound = pValuesSorted( rFDR-1 );
        disp( 'FDR passed' )
    else
        pValueBound = pValuesSorted( 1 );
        disp( 'FDR failed' )
    end
    voxelsInsignificant =  pValueContainerBetterTrimmed >= pValueBound ;
    hContainerBetterTrimmed( voxelsInsignificant ) = false;
    
    for indexVoxel = 1:numel( voxelsToTestTrimmed )
        hContainerBetter( voxelsToTest( indexVoxel )) = ...
            hContainerBetterTrimmed( indexVoxel );
%         hContainerWorse( voxelsToTest( indexVoxel )) = ...
%             hContainerWorseTrimmed( indexVoxel );
    end
    
    mapSignificantBetter = mapProbabilistic;
    mapSignificantBetter.img = zeros( mapSizeX, mapSizeY, mapSizeZ );
    voxelsSignificantBetter = find( hContainerBetter == 1 );
    mapSignificantBetter.img( voxelsSignificantBetter ) = mapProbabilistic.img( voxelsSignificantBetter );
    mapSignificantBetterBinary = mapSignificantBetter;
    mapSignificantBetterBinary.img( voxelsSignificantBetter ) = 1;
    
    [ xx, yy, zz ] = ind2sub( size( mapSignificantBetterBinary.img ), voxelsSignificantBetter );
    voxelCoordinates = [ xx, yy, zz ];
    worldCoordinates = mapVoxelToWorld( voxelCoordinates, mapSignificantBetterBinary );
    coordinatesSignificantBetter = mean( worldCoordinates );
    
    if ( plotImages ) && ( ~isempty( voxelsSignificantBetter ) )
        figure( 'color', 'w' )
        trisurf( boundarySTNRight, ptCloud.Location( ( Idx & IdxRight ), 1 ), ...
            ptCloud.Location( ( Idx & IdxRight ) , 2 ), ...
            ptCloud.Location( ( Idx & IdxRight ) , 3 ), 'Facecolor', [ .4 .4 .4 ],...
            'FaceAlpha', 0.2, 'LineStyle' , 'none' , 'EdgeColor' , pcColors( colorIdx, : ) );
        hold on
        [ xx, yy, zz] = ind2sub( size( mapSignificantBetter.img ), voxelsSignificantBetter );
        if ~isempty( xx )
            voxelCoordinates = [ xx, yy, zz ]; % concatenate points to one matrix.
            worldCoordinates = mapVoxelToWorld( voxelCoordinates, mapSignificantBetter ); % map to mm-space
        end
        h = scatter3( ...
            worldCoordinates( :,1 ), ...
            worldCoordinates( :,2 ), ...
            worldCoordinates( :,3 ), ...
            markerSize,...
            mapSignificantBetter.img( voxelsSignificantBetter ),'filled' );
        h.MarkerFaceAlpha = 0.4;
        
        view( [ -30, 30 ] )
        cb = colorbar;
        maxEffect = max( mapSignificantBetter.img( voxelsSignificantBetter ) );
        caxis( [ 0 maxEffect ] )
        cb.Label.String = 'Mean clinical improvement';
        title( 'Significant better clinical improvement' )
        
        xlabel('Medial-lateral (mm)')
        ylabel('Posterior-anterior (mm)')
        zlabel('Inferior-superior (mm)')
        grid on
        xlim([0 20]); ylim([-20 -5]); zlim([-15 0]);
    end
    
    close( fWaitStatistics )
    %% Overlap
    overlap = zeros( height( tableTesting ), 5 );
    distance = zeros( height( tableTesting ), 2 );
    
    for indexVTA = 1:height( tableTesting )
        fileNameVTA = tableTesting.fileNames{ indexVTA };
        VTATest = ea_load_nii( fileNameVTA );
        indexLinearVTA = find( VTATest.img >= 0.33 );
        indexLinearVTADiscard = find( VTATest.img < 0.33 );
        VTATest.img( indexLinearVTA ) = 1;
        VTATest.img( indexLinearVTADiscard ) = 0;
        overlap( indexVTA, 1 ) = calcOverlapRatio( VTATest, mapSignificantBetterBinary );
        overlap( indexVTA, 2 ) = calcOverlapRatio( VTATest, tractCologne );
        overlap( indexVTA, 3 ) = calcOverlapVolume( VTATest, tractCologne )*calcOverlapSum( VTATest, tractCologne );
        overlap( indexVTA, 4 ) = calcOverlapVolume( VTATest, tractCologne );
        overlap( indexVTA, 5 ) = calcOverlapScore( VTATest, tractCologne );

        [ xx, yy, zz ] = ind2sub( size( VTATest.img ), indexLinearVTA );
        VTAVoxelCoordinates = [ xx, yy, zz ];
        VTAWorldCoordinates = mapVoxelToWorld( VTAVoxelCoordinates, VTATest );
        meanVTACoordinates = mean( VTAWorldCoordinates );
        distance( indexVTA, 1 ) = norm( ( meanVTACoordinates - coordinatesSignificantBetter ) );
        distance( indexVTA, 2 ) = norm( ( meanVTACoordinates - coordinatesClassicVIM ) );
    end
    crossValidationResults( test( crossValidationPartition, indexFold ), : ) = ...
        [ distance overlap ];
%     if ~isnan( distance( 1, 1) )
%         fitlm( distance( :, 1 ), tableTesting.tremorReductionPartA )
%     end
    toc
end % fold


% toc 
% format shortg
% c = clock
