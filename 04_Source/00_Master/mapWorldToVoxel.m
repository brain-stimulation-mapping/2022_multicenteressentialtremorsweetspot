function voxelCoordinates = mapWorldToVoxel(worldCoordinates,image)
worldCoordinates = [worldCoordinates';ones(1,size(worldCoordinates,1))];
voxelCoordinates = image.mat\worldCoordinates;
voxelCoordinates = round(voxelCoordinates);
% voxelCoordinates(voxelCoordinates<1) = 1;
voxelCoordinates = voxelCoordinates(1:3,:)';
%% control, remove coordinates outside image
% voxelCoordinates( voxelCoordinates(:,1) < 1, : ) = [];
% voxelCoordinates( voxelCoordinates(:,2) < 1, : ) = [];
% voxelCoordinates( voxelCoordinates(:,3) < 1, : ) = [];
% voxelCoordinates( voxelCoordinates(:,1) > image.dim(1), : ) = [];
% voxelCoordinates( voxelCoordinates(:,2) > image.dim(2), : ) = [];
% voxelCoordinates( voxelCoordinates(:,3) > image.dim(3), : ) = [];
end