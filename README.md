# 2022_MulticenterEssentialTremorSweetSpot

Nowacki et al. (2022), Probabilistic Mapping Reveals Optimal Stimulation Site in Essential Tremor, https://doi.org/10.1002/ana.26324 .

This repository has the analysis scripts only but not the Nifti files with the volumes of tissue activated. Please contact the main author, if you would like to have this. The analysis scripts are the final version used for the paper and the repository does not include the earlier revisions.
